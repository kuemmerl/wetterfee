#!/usr/bin/env python3

# MIT License
# Copyright (c) 2022 Rainer Kuemmerle
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import time
import board
import adafruit_dht
import yaml
import pydantic
from tinydb import TinyDB, Query
from datetime import datetime, timedelta, timezone
from typing import Iterator, Dict, List
import re


TIMEDELTA_REGEX = (
    r"((?P<days>-?\d+)d)?" r"((?P<hours>-?\d+)h)?" r"((?P<minutes>-?\d+)m)?"
)
TIMEDELTA_PATTERN = re.compile(TIMEDELTA_REGEX, re.IGNORECASE)


def parse_delta(delta) -> timedelta:
    """Parses a human readable timedelta (3d5h19m) into a datetime.timedelta.
    Delta includes:
    * Xd days
    * Xh hours
    * Xm minutes
    Values can be negative following timedelta's rules. Eg: -5h-30m
    """
    match = TIMEDELTA_PATTERN.match(delta)
    if match:
        parts = {k: int(v) for k, v in match.groupdict().items() if v}
        return timedelta(**parts)
    else:
        return timedelta()


class SensorReading(pydantic.BaseModel):
    timestamp: float
    temperature: float
    humidity: float


class WetterFee:
    def __init__(self, config_filename) -> None:
        self.config = self._read_config(config_filename)
        self.db: TinyDB = None

    def _read_config(self, filename) -> Dict[str, str]:
        with open(filename) as f:
            return yaml.safe_load(f)

    def open_db(self) -> None:
        if not self.db:
            self.db = TinyDB(self.config["DB"])

    def insert(self, value: SensorReading) -> None:
        self.db.insert(value.dict())

        now = datetime.now(timezone.utc).timestamp()
        max_age = parse_delta(self.config["HISTORY"]).total_seconds()

        entry = Query()
        entries_too_old = self.db.search(entry.timestamp < now - max_age)
        self.db.remove(doc_ids=[e.doc_id for e in entries_too_old])

    def insert_beebotte(self, value: SensorReading) -> None:
        from beebotte import BBT, Resource

        bbt = BBT(token=self.config["BEE_TOKEN"])
        temp_resource = Resource(bbt, self.config["BEE_CHANNEL"], "temperature")
        humid_resource = Resource(bbt, self.config["BEE_CHANNEL"], "humidity")

        # TODO Use bulkWrite for both values in one POST
        # TODO pass timestamp to beebotte
        try:
            temp_resource.write(value.temperature)
            humid_resource.write(value.humidity)
        except Exception as e:
            print("Error while writing to Beebotte")
            print(e)

    def read_dht(self, retries: int = 5) -> SensorReading:
        sensor_to_use = self.config["SENSOR"]
        dht_device = None
        if sensor_to_use == "DHT11":
            dht_device = adafruit_dht.DHT11(board.D4, use_pulseio=True)
        elif sensor_to_use == "DHT22":
            dht_device = adafruit_dht.DHT22(board.D4, use_pulseio=True)

        if not dht_device:
            return SensorReading(
                timestamp=0.0, temperature=float("inf"), humidity=float("inf")
            )


        try:
            for _ in range(retries):
                try:
                    temperature = dht_device.temperature
                    humidity = dht_device.humidity
                    timestamp = datetime.now(timezone.utc).timestamp()
                    return SensorReading(
                        timestamp=timestamp, temperature=temperature, humidity=humidity
                    )
                except RuntimeError as error:
                    # reading errors happen often. Hence we retry after some time.
                    time.sleep(2.0)
                    continue
        except Exception as error:
            raise error
        finally:
            dht_device.exit()

        # was unable to read anything
        return SensorReading(
            timestamp=0.0, temperature=float("inf"), humidity=float("inf")
        )

    def plot_data(self) -> Iterator[SensorReading]:
        now = datetime.now(timezone.utc).timestamp()
        max_age = parse_delta(self.config["PLOT_SUMMARY"]).total_seconds()

        entry = Query()
        entries_to_plot = self.db.search(entry.timestamp > now - max_age)
        for e in entries_to_plot:
            yield SensorReading.parse_obj(e)

    def dump_plot(self) -> bool:
        import matplotlib.pyplot as plt

        dates: List[datetime] = []
        temperature: List[float] = []
        humidity: List[float] = []

        for reading in self.plot_data():
            dates.append(datetime.fromtimestamp(reading.timestamp, timezone.utc))
            temperature.append(reading.temperature)
            humidity.append(reading.humidity)

        _, ax1 = plt.subplots()
        ax1.tick_params(axis="x", rotation=45)

        ax2 = ax1.twinx()
        ax1.plot(dates, temperature, "r-")
        ax2.plot(dates, humidity, "b-")

        ax1.set_xlabel("Datum")
        ax1.set_ylabel("Temperatur", color="r")
        ax2.set_ylabel("Feuchtigkeit", color="b")

        plt.savefig(self.config["PLOT_FILENAME"], bbox_inches="tight")

        return True

    def send_mail(self) -> bool:
        import smtplib, ssl
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.mime.image import MIMEImage

        smtp_server = self.config["SMTP_HOST"]
        port = int(self.config["SMTP_PORT"])
        sender_email = self.config["SMTP_USER"]
        password = self.config["SMTP_PASSWORD"]
        receiver_email = self.config["RECEIVER"]

        # Create a multipart message and set headers
        message = MIMEMultipart()
        message["From"] = sender_email
        message["To"] = receiver_email
        message["Subject"] = "WetterFee Status Report"

        # text of our mail
        body = 'WetterFee Status Report<br><img src="cid:image1">'

        # append IP addresses as list to mail
        try:
            ips = self.read_ips()
            if ips:
                body += "\n<br><br>\nIP Addresses:"
                body += "<ul>\n"
                for ip in ips:
                    body += f"<li>{ip}</li>"
                body += "</ul>\n"
        except:
            pass

        # Add body to email
        message.attach(MIMEText(body, "html"))

        # Open file in binary mode
        filename = self.config["PLOT_FILENAME"]
        with open(filename, "rb") as attachment:
            image = MIMEImage(attachment.read())
            image.add_header("Content-ID", "<image1>")
            message.attach(image)

        # sending the mail by smtp
        context = ssl.create_default_context()
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls(context=context)
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message.as_string())

        return True

    def read_ips(self) -> List[str]:
        from requests import get

        ip4 = yaml.safe_load(get("https://api4.ipify.org?format=json").text)
        ip6 = yaml.safe_load(get("https://api6.ipify.org?format=json").text)
        return [ip4["ip"], ip6["ip"]]

    def has_internet(self):
        import http.client as httplib

        conn = httplib.HTTPSConnection("8.8.8.8", timeout=5)
        try:
            conn.request("HEAD", "/")
            return True
        except Exception:
            return False
        finally:
            conn.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog="WetterFee", allow_abbrev=False)
    parser.add_argument(
        "-p",
        "--print",
        action="store_true",
        help="Take a measurement and print to stdout, exit afterwards",
    )
    parser.add_argument(
        "-m", "--measure", action="store_true", help="Take a measurement and add to DB"
    )
    parser.add_argument("-s", "--send", action="store_true", help="Send report by mail")
    parser.add_argument("-d", "--dump", action="store_true", help="Dump a plot")
    parser.add_argument(
        "-c", "--check_internet", action="store_true", help="Check internet conncetion"
    )
    args = parser.parse_args()

    wetterfee = WetterFee("config.yaml")

    if args.print:
        print(f"Values: {wetterfee.read_dht()}")
        exit(0)

    if args.check_internet:
        online_cnt = -1
        for i in range(10):
            if wetterfee.has_internet():
                online_cnt = i
                break
        if online_cnt >= 0:
            print(f"WetterFee is online, check {online_cnt+1} suceeded")
        else:
            print("WetterFee is offline :-(")

    if args.measure:
        value = wetterfee.read_dht()
        wetterfee.open_db()
        wetterfee.insert(value)
        wetterfee.insert_beebotte(value)

    if args.dump:
        wetterfee.open_db()
        wetterfee.dump_plot()

    if args.send:
        wetterfee.open_db()
        wetterfee.dump_plot()
        wetterfee.send_mail()
