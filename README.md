# wetterfee

Read data out of a DHT11 and populate a database of measurements.
Supports sending a plot of the data by mail.

## Example configuration

The wetterfee uses a config.yaml for configuration.

```
# Mail configuration
SMTP_HOST: mail.example.com
SMTP_PORT: 587
SMTP_USER: user@example.com
SMTP_PASSWORD: super_secret_pw
RECEIVER: receiver@example.com
# Database and history
DB: db.json
HISTORY: 14d
# Plotting
PLOT_SUMMARY: 7d
PLOT_FILENAME: wetterfee.png
```

## crontab

Record a measurement every 10 minutes and send a plot at 7pm each day.

```
*/10 * * * * python3 wetterfee.py -m
5 19 * * * python3 wetterfee.py -s
```

